This script automatically configures WindowsFX instalation on a RaspberryPI 4

# Install WindowsFX
- burn image on a SD card (windowsfx10.1-armhf-rpi.xz)

# Configure Raspberry PI
- upon installation completed:
    - set country and timezone 
    - skip update software
    - set wireless password
    - disable Bluetooth from startup applications
- open a terminal and run the following commands:
    - sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes' /etc/ssh/sshd_config
    - sudo raspi-config
        - resize root partition (Advanced Options/Expand Filesystem)
        - enable SSH on boot
- install software
    - curl -L https://bitbucket.org/lirimia/windowsfx_rpi/raw/master/setup.sh | bash
