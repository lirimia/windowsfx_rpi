echo
echo "**************************************************"
echo "* Update linux                                   *"
echo "**************************************************"
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
sudo -E apt-get -qy update
sudo -E apt-get -qy -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade
sudo -E apt-get -qy autoclean


echo
echo "**************************************************"
echo "* Install and configure git                      *"
echo "**************************************************"
sudo apt-get install -y git qgit

git config --global user.name 'Laur'
git config --global user.email 'lirimia@gmail.com'


echo
echo "**************************************************"
echo "* Install system tools                           *"
echo "**************************************************"
sudo apt-get install -y mc 
sudo apt-get install -y transmission-gtk
sudo apt-get install -y filezilla
sudo apt-get install -y putty

echo
echo "**************************************************"
echo "* Install Chrome                                 *"
echo "**************************************************"
sudo apt-get install -y chromium-browser


echo "**************************************************"
echo "* Install webcam software                        *"
echo "**************************************************"
# https://www.instructables.com/How-to-Make-Raspberry-Pi-Webcam-Server-and-Stream-/
sudo apt-get install -y motion

sudo sed -i 's/daemon off/daemon on/' /etc/motion/motion.conf
sudo sed -i 's/framerate 15/framerate 1000/' /etc/motion/motion.conf
sudo sed -i 's/stream_localhost on/stream_localhost off/' /etc/motion/motion.conf
sudo sed -i 's/webcontrol_localhost on/webcontrol_localhost off/' /etc/motion/motion.conf
sudo sed -i 's/movie_quality 45/movie_quality 100/' /etc/motion/motion.conf
sudo sed -i 's/post_capture 0/post_capture 5/' /etc/motion/motion.conf

sudo sed -i 's/start_motion_daemon=no/start_motion_daemon=yes/' /etc/default/motion


echo
echo "**************************************************"
echo "* Enable autologin                               *"
echo "**************************************************"
echo "autologin-user=admin" | sudo tee -a /etc/lightdm/lightdm.conf
echo "autologin-user-timeout=0" | sudo tee -a /etc/lightdm/lightdm.conf

sudo rm -rf .local/share/keyrings/*.keyring

sudo reboot

# upon reboot set WiFi password and set an empty password for login keyring